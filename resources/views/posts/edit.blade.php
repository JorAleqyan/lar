@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Post</div>
                    <div class="card-body">
                        <form action="{{route('posts.update', $post->id )}}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{method_field('patch')}}
                            <input type="text" name="title" placeholder="Name" value="{{$post->title}}" class="form-control">
                            <textarea name="context" placeholder="Description" cols="30" rows="10" class="form-control">{{$post->context}}</textarea>
                            <input type="submit" class="btn btn-success" value="Update">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
