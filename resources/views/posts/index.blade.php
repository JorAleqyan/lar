@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header">Post
                        <a href="/posts/create" class="btn btn-primary float-right">Add new</a>
                    </div>

                    <div class="card-header">
                        <form action="/posts" method="get">
                            <input type="search" name="search" class="form-control" value="{{$search}}"><br>
                            <select  name="order" class="form-control">
                                <option value="asc">
                                    Order By Title
                                </option>
                                <option value="desc">
                                    Order By Title desc
                                </option>
                            </select>
                            <button class="btn btn-success">Search</button>
                        </form>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th>Title</th>
                                <th>Context</th>
                            </tr>
                            @foreach($posts as $item)
                                <tr>
                                    <td>{{$item->title}}</td>
                                    <td>{{$item->context}}</td>
                                    <td>
                                        <div class="d-flex">
                                            <a href="/posts/{{$item->id}}" class="btn btn-primary">Show</a>
                                            <a href="{{route('posts.edit', $item->id)}}" class="btn btn-success">Edit</a>
                                            <form action="{{route('posts.destroy', $item->id)}}" method="post">
                                                @csrf
                                                {{method_field('DELETE')}}
                                                <button class="btn btn-danger" >Delete</button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $posts->appends(Request::except('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

