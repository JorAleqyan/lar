@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Post</div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th>Title</th>
                                <td>{{$post->title}}</td>
                            </tr>
                            <tr>
                                <th>Context</th>
                                <td>{{$post->context}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
