@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Post</div>
                    <div class="card-body">
                        <form action="/posts" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="text" name="title" placeholder="Title" class="form-control">
                            <textarea name="context" placeholder="Context" cols="30" rows="10" class="form-control"></textarea>
                            <input type="submit" class="btn btn-success" value="Create">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
