@extends('layouts.app')
@section('content')
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Products <a href="/products/create" class="btn btn-primary float-right">Add new</a></div>

                        <div class="card-body">

                            <form action="/products" method="get">
                                <input type="search" name="search" class="form-control" value="{{$search}}">
                                <div class="d-flex">
                                <input type="number" name="price_from" class="form-control">
                                <input type="number" name="price_to" class="form-control">
                                </div>
                                <button class="btn btn-success">Search</button>
                            </form>

                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Actions</th>
                                </tr>
                                @foreach($products as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->price}}</td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="/products/{{$item->id}}" class="btn btn-primary">Show</a>
                                                <a href="{{route('products.edit', $item->id)}}" class="btn btn-success">Edit</a>
                                                <form action="{{route('products.destroy', $item->id)}}" method="post">
                                                    @csrf
                                                    {{method_field('DELETE')}}
                                                    <button class="btn btn-danger" >Delete</button>
                                                </form>
                                            </div>

                                        </td>
                                    </tr>
                                    @endforeach
                            </table>
                            {{$products->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

