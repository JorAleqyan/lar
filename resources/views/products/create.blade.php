@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Products</div>

                    <div class="card-body">


                        <form action="/products" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="text" name="name" placeholder="Name" class="form-control">
                            <textarea name="description" placeholder="Description" cols="30" rows="10" class="form-control"></textarea>
                            <input type="number" name="price" placeholder="Price" class="form-control">
                            <input type="file" name="image" class="form-control">
                            <input type="submit" class="btn btn-success" value="Create">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
