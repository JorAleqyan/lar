@extends('layouts.app')
@section('content')
    <table class="table">
        <tr>
            <th>Title</th>
            <th>Content</th>
            <th>Action</th>
        </tr>
        @foreach($articles as $item)
            <tr>
                <td>{{$item->title}}</td>
                <td>{{$item->content}}
                <td>
                    <div class="d-flex">
                        <a href="{{route('article.edit',$item->id)}}" type="button" class="btn btn-success">Edit</a>
                        <form action="{{route('article.destroy',$item->id)}}" method="post">
                            {{csrf_field()}}
                            {{ method_field('DELETE') }}
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach

    </table>



@endsection