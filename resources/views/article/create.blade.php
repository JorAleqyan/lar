@extends('layouts.app')
@section('content')
<form action="{{route('article.store')}}" method="post" class="w-50 mx-auto alert">
    {{csrf_field()}}
    <input type="text" placeholder="Title" name="title" class="form-control">
    <textarea name="content" id="" cols="30" rows="10" placeholder="Content" class="form-control"></textarea>
    <button class="btn btn-success">Create</button>
</form>
@endsection