@extends('layouts.app')
@section('content')

    <form action="{{route('article.update',$article->id)}}" method="post" class="w-50 mx-auto alert">
        {{csrf_field()}}
        {{ method_field('PUT')}}
        <input type="text" placeholder="{{$article->title}}" name="title" class="form-control">
        <textarea name="content" id="" cols="30" rows="10" placeholder="{{$article->content}}" class="form-control"></textarea>
        <button class="btn btn-success">Update</button>
    </form>

@endsection